"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Schema_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Lucid/Schema"));
class OtpCodes extends Schema_1.default {
    constructor() {
        super(...arguments);
        this.tableName = 'otp_codes';
    }
    async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('otp_code');
            table.integer('user_id').unsigned().references('id').inTable('users');
            table.dateTime('created_at', { useTz: true });
            table.dateTime('updated_at', { useTz: true });
        });
    }
    async down() {
        this.schema.dropTable(this.tableName);
    }
}
exports.default = OtpCodes;
//# sourceMappingURL=1632406984614_otp_codes.js.map