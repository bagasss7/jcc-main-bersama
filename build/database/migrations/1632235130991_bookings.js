"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Schema_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Lucid/Schema"));
class Bookings extends Schema_1.default {
    constructor() {
        super(...arguments);
        this.tableName = 'bookings';
    }
    async up() {
        this.schema.createTable(this.tableName, (table) => {
            table.increments('id');
            table.integer('field_id').unsigned().references('fields.id').onDelete('CASCADE');
            table.dateTime('play_date_start').notNullable();
            table.dateTime('play_date_end').notNullable();
            table.integer('user_id').unsigned().references('users.id').onDelete('CASCADE');
        });
    }
    async down() {
        this.schema.dropTable(this.tableName);
    }
}
exports.default = Bookings;
//# sourceMappingURL=1632235130991_bookings.js.map