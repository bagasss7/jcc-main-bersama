"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Verify {
    async handle({ auth, response }, next) {
        const user = auth.user?.isVerified;
        if (user) {
            await next();
        }
        else {
            return response.unauthorized({ message: 'user belum terverifikasi!' });
        }
    }
}
exports.default = Verify;
//# sourceMappingURL=Verify.js.map