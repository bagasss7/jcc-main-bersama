"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Rbac {
    async handle({ auth, response }, next, rules) {
        const roles = rules;
        if (roles.length === 0) {
            return response.badRequest({ message: `err` });
        }
        else {
            try {
                const user = auth.user?.role;
                if (roles.includes(user)) {
                    await next();
                }
                else {
                    return response.unauthorized({ message: `Hanya user dengan role ${roles} yang dapat akses route ini` });
                }
            }
            catch (err) {
                return response.badRequest({ message: `${err}` });
            }
        }
    }
}
exports.default = Rbac;
//# sourceMappingURL=Rbac.js.map