"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class CreateContactValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            name: Validator_1.schema.string({}, [
                Validator_1.rules.alpha({
                    allow: ['space']
                })
            ]),
            address: Validator_1.schema.string({}, []),
            phone: Validator_1.schema.string({}, [
                Validator_1.rules.mobile()
            ])
        });
        this.messages = {
            'required': 'the {{field}} is required',
            'name.alpha': 'the {{field}} must be characters without number or symbol',
            'phone.mobile': 'the {{field}} must be a phone number format'
        };
    }
}
exports.default = CreateContactValidator;
//# sourceMappingURL=CreateContactValidator.js.map