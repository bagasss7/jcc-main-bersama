"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class BookByVenueValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            field_id: Validator_1.schema.number(),
            play_date_start: Validator_1.schema.date({}, [
                Validator_1.rules.after('today')
            ]),
            play_date_end: Validator_1.schema.date({}, [
                Validator_1.rules.after('today')
            ])
        });
        this.messages = {};
    }
}
exports.default = BookByVenueValidator;
//# sourceMappingURL=BookByVenueValidator.js.map