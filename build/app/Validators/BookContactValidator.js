"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class BookContactValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            play_date_start: Validator_1.schema.date({}, [
                Validator_1.rules.after('today')
            ]),
            play_date_end: Validator_1.schema.date({}, [
                Validator_1.rules.after('today')
            ])
        });
        this.messages = {
            'required': 'the {{field}} is required',
            'name.alpha': 'the {{field}} must be characters without number or symbol',
            'waktu.after': 'You must book the place atleast a day before!'
        };
    }
}
exports.default = BookContactValidator;
//# sourceMappingURL=BookContactValidator.js.map