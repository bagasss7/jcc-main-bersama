"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
class FieldsValidator {
    constructor(ctx) {
        this.ctx = ctx;
        this.schema = Validator_1.schema.create({
            name: Validator_1.schema.string({}, [
                Validator_1.rules.alpha({
                    allow: ['space']
                })
            ]),
            type: Validator_1.schema.enum(['futsal', 'mini soccer', 'basketball', 'soccer', 'volleyball'])
        });
        this.messages = {
            'required': 'the {{field}} is required',
            'name.alpha': 'the {{field}} must be characters without number or symbol',
            'phone.mobile': 'the {{field}} must be a phone number format'
        };
    }
}
exports.default = FieldsValidator;
//# sourceMappingURL=FieldsValidator.js.map