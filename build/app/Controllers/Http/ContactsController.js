"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const CreateContactValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/CreateContactValidator"));
const Venue_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Venue"));
const Booking_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Booking"));
const BookByVenueValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/BookByVenueValidator"));
class ContractsController {
    async index({ request, response }) {
        try {
            let idSearch = request.qs().id;
            if (idSearch) {
                let venueFilter = await Venue_1.default.find(idSearch);
                return response.status(200).json({ message: 'Sukses mengambil data', data: venueFilter });
            }
            else {
                let venueFind = await Venue_1.default.all();
                return response.status(200).json({ message: 'Sukses mengambil data', data: venueFind });
            }
        }
        catch (err) {
            return response.badRequest({ errors: err.messages });
        }
    }
    async store({ request, response }) {
        try {
            let payload = await request.validate(CreateContactValidator_1.default);
            let venue = await Venue_1.default.create(payload);
            return response.created({ message: 'berhasil menambah Venue', newId: venue });
        }
        catch (err) {
            return response.unprocessableEntity({ message: 'gagal menambah Venue' });
        }
    }
    async show({ params, response }) {
        try {
            const venue = await Venue_1.default.query().where('id', params.id).preload('fields', (query) => {
                query.preload('booking');
            }).firstOrFail();
            const { id, name, address, phone, fields } = venue;
            return response.ok({ status: 'berhasil get data venue by id', data: { id, name, address, phone, fields } });
        }
        catch (err) {
            return response.badRequest({ errors: `${err}` });
        }
    }
    async update({ request, response, params }) {
        try {
            let id = params.id;
            let venueFilter = await Venue_1.default.findOrFail(id);
            venueFilter.name = request.input('name');
            venueFilter.address = request.input('address');
            venueFilter.phone = request.input('phone');
            venueFilter.save();
            return response.ok({ message: 'updated!', data: venueFilter });
        }
        catch (err) {
            return response.badRequest({ errors: `${err}` });
        }
    }
    async destroy({ params, response }) {
        try {
            let venueDelete = await Venue_1.default.findOrFail(params.id);
            await venueDelete.delete();
            return response.ok({ message: 'data deleted!' });
        }
        catch (err) {
            return response.badRequest({ message: `Data tidak ditemukan!` });
        }
    }
    async booking({ request, response, auth, params }) {
        try {
            const fieldId = request.input('field_id');
            const venue = await Venue_1.default.find(params.id);
            const field = await venue.related('fields').query().where('id', fieldId);
            if (field.length === 0) {
                return response.badRequest({ message: 'field tidak ada di venue ini' });
            }
            const user = auth.user;
            const payload = await request.validate(BookByVenueValidator_1.default);
            const newBooking = new Booking_1.default();
            newBooking.play_date_start = payload.play_date_start;
            newBooking.play_date_end = payload.play_date_end;
            await newBooking.related('field').associate(field[0]);
            user.related('myBookings').save(newBooking);
            return response.created({ message: 'berhasil booking' });
        }
        catch (err) {
            return response.unprocessableEntity({ errors: 'tidak ada field di venue tsb' });
        }
    }
}
exports.default = ContractsController;
//# sourceMappingURL=ContactsController.js.map