"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Validator_1 = global[Symbol.for('ioc.use')]("Adonis/Core/Validator");
const User_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/User"));
const UserValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/UserValidator"));
const Mail_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Addons/Mail"));
const Database_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Lucid/Database"));
class AuthController {
    async register({ request, response }) {
        try {
            const data = await request.validate(UserValidator_1.default);
            const newUser = await User_1.default.create(data);
            const otp_code = Math.floor(100000 + Math.random() * 900000);
            await Database_1.default.table('otp_codes').insert({ otp_code, user_id: newUser.id });
            await Mail_1.default.send((message) => {
                message
                    .from('admin@venueapi.com')
                    .to(data.email)
                    .subject('Welcome Onboard!')
                    .htmlView('emails/otp_verification', { otp_code });
            });
            return response.created({ message: "User berhasil register, verifiy akunmu!" });
        }
        catch (err) {
            return response.badRequest({ message: `${err}` });
        }
    }
    async login({ request, response, auth }) {
        try {
            const userSchema = Validator_1.schema.create({
                email: Validator_1.schema.string(),
                password: Validator_1.schema.string()
            });
            await request.validate({ schema: userSchema });
            const email = request.input('email');
            const password = request.input('password');
            const token = await auth.use('api').attempt(email, password);
            return response.ok({ message: 'login berhasil!', token });
        }
        catch (err) {
            return response.badRequest({ message: `${err}` });
        }
    }
    async otpConfirmation({ request, response }) {
        try {
            const otp_code = request.input('otp_code');
            const email = request.input('email');
            const findUser = await User_1.default.findBy('email', email);
            const otpCheck = await Database_1.default.query().from('otp_codes').where('otp_code', otp_code).first();
            if (findUser?.id === otpCheck.user_id) {
                findUser.isVerified = true;
                await findUser?.save();
                return response.status(200).json({ message: 'Berhasil konfirmasi OTP' });
            }
            else {
                return response.status(400).json({ message: 'Gagal verifikasi OTP' });
            }
        }
        catch (err) {
            return response.badRequest({ message: `${err}` });
        }
    }
}
exports.default = AuthController;
//# sourceMappingURL=AuthController.js.map