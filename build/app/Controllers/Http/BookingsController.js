"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Booking_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Booking"));
const Field_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Field"));
const BookContactValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/BookContactValidator"));
class BookingsController {
    async index({ response }) {
        const allBook = await Booking_1.default.query().select('id', 'play_date_start', 'play_date_end', 'user_id', 'field_id').preload('field', (query) => {
            query.preload('venue');
        });
        return response.status(200).json({ message: 'Sukses mengambil data seluruh Booking', data: allBook });
    }
    async create({}) {
    }
    async store({ request, response, params, auth }) {
        try {
            const field = await Field_1.default.findByOrFail('id', params.field_id);
            const user = auth.user;
            const payload = await request.validate(BookContactValidator_1.default);
            const newBooking = new Booking_1.default();
            newBooking.play_date_start = payload.play_date_start;
            newBooking.play_date_end = payload.play_date_end;
            newBooking.related('field').associate(field);
            user.related('myBookings').save(newBooking);
            return response.created({ message: 'berhasil booking' });
        }
        catch (err) {
            return response.badRequest({ err: `${err}` });
        }
    }
    async show({ params, response }) {
        try {
            const books = await Booking_1.default.query().where('id', params.id).withCount('players').preload('players', (playerQuery) => {
                playerQuery.select(['id', 'name', 'email']);
            }).firstOrFail();
            const { id, field_id, play_date_start, play_date_end, user_id, players } = books.toJSON();
            const players_count = books.$extras.players_count;
            return response.ok({ status: 'berhasil get data booking by id',
                data: id, field_id, play_date_start, play_date_end, booking_user_id: user_id, players_count, players });
        }
        catch (err) {
            return response.badRequest({ err: `${err}` });
        }
    }
    async edit({}) {
    }
    async update({}) {
    }
    async destroy({}) {
    }
    async join({ params, response, auth }) {
        try {
            const book = await Booking_1.default.findByOrFail('id', params.id);
            const user = auth.user;
            await book.related('players').sync([user.id], false);
            return response.created({ message: 'berhasil join booking' });
        }
        catch (err) {
            return response.badRequest({ err: `${err}` });
        }
    }
    async unjoin({ params, response, auth }) {
        try {
            const booking = await Booking_1.default.findOrFail(params.id);
            const user = auth.user;
            await booking.related('players').detach([user.id]);
            return response.created({ message: 'berhasil unjoin booking' });
        }
        catch (err) {
            return response.badRequest({ err: `${err}` });
        }
    }
    async schedules({ response, auth }) {
        try {
            const user = auth.user;
            const books = await Booking_1.default.query().select('id', 'play_date_start', 'play_date_end', 'user_id', 'field_id').preload('field', (query) => {
                query.preload('venue');
            }).whereHas('players', (query) => {
                query.where('user_id', user.id);
            }).orWhere('id', user.id);
            return response.ok({ status: 'berhasil get schedule booking', books });
        }
        catch (err) {
            return response.badRequest({ err: `Belum ada book` });
        }
    }
}
exports.default = BookingsController;
//# sourceMappingURL=BookingsController.js.map