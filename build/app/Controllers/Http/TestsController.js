"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class TestsController {
    async hello({ request, response }) {
        const name = request.input('name', 'Guess');
        response.send({ message: 'Hello ' + name });
    }
}
exports.default = TestsController;
//# sourceMappingURL=TestsController.js.map