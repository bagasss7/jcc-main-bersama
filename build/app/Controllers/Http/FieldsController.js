"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const FieldsValidator_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Validators/FieldsValidator"));
const Field_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Field"));
const Venue_1 = __importDefault(global[Symbol.for('ioc.use')]("App/Models/Venue"));
class FieldsController {
    async index({ params, response }) {
        try {
            let showFields = await Field_1.default.query().where('venue_id', params.venue_id);
            return response.status(200).json({ message: 'Sukses mengambil data Fields dari Venue', data: showFields });
        }
        catch (err) {
            return response.badRequest({ errors: `${err}` });
        }
    }
    async store({ params, request, response }) {
        try {
            let payload = await request.validate(FieldsValidator_1.default);
            const venue = await Venue_1.default.findOrFail(params.venue_id);
            const newField = new Field_1.default();
            Object.assign(newField, payload);
            await newField.related('venue').associate(venue);
            return response.created({ message: 'berhasil menambahkan data field baru' });
        }
        catch (err) {
            return response.badRequest({ errors: `${err}` });
        }
    }
    async show({ params, response }) {
        try {
            let id = params.id;
            let fieldsFilter = await Field_1.default.query().where('venue_id', params.venue_id).andWhere('id', id).firstOrFail();
            return response.status(200).json({ message: 'Sukses mengambil field', data: fieldsFilter });
        }
        catch (err) {
            return response.badRequest({ errors: 'field tidak ada' });
        }
    }
    async update({ request, response, params }) {
        try {
            let id = params.id;
            let fieldsFilter = await Field_1.default.query().where('venue_id', params.venue_id).andWhere('id', id).firstOrFail();
            fieldsFilter.name = request.input('name');
            fieldsFilter.type = request.input('type');
            fieldsFilter.venueId = request.input('venue_id');
            fieldsFilter.save();
            return response.ok({ message: 'updated!', data: fieldsFilter });
        }
        catch (err) {
            return response.badRequest({ errors: `${err}` });
        }
    }
    async destroy({ params, response }) {
        try {
            let fieldsFilter = await Field_1.default.query().where('venue_id', params.venue_id).andWhere('id', params.id).firstOrFail();
            await fieldsFilter.delete();
            return response.ok({ message: 'data deleted!' });
        }
        catch (err) {
            return response.badRequest({ message: `Data tidak ditemukan!` });
        }
    }
    async showField({ params, response }) {
        try {
            let id = params.id;
            const field = await Field_1.default.query().where('id', id).preload('venue').preload('booking').firstOrFail();
            return response.ok({ status: 'berhasil get data booking', data: field });
        }
        catch (err) {
            return response.badRequest({ message: `${err}` });
        }
    }
}
exports.default = FieldsController;
//# sourceMappingURL=FieldsController.js.map