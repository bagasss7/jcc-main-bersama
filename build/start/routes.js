"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Route_1 = __importDefault(global[Symbol.for('ioc.use')]("Adonis/Core/Route"));
Route_1.default.post('/register', 'AuthController.register').as('auth.register');
Route_1.default.post('/login', 'AuthController.login').as('auth.login');
Route_1.default.post('/verifikasi-otp', 'Authcontroller.otpConfirmation').as('auth.otp');
Route_1.default.get('/api/hello', 'TestsController.hello');
Route_1.default.group(() => {
    Route_1.default.group(() => {
        Route_1.default.get('/venues', 'ContactsController.index').as('venues.index');
        Route_1.default.post('/venues', 'ContactsController.store').as('venues.store');
        Route_1.default.get('/venues/:id', 'ContactsController.show').as('venues.show');
        Route_1.default.put('/venues/:id', 'ContactsController.update').as('venues.update');
        Route_1.default.delete('/venues/:id', 'ContactsController.destroy').as('venues.destroy');
        Route_1.default.resource('venues.fields', 'FieldsController');
        Route_1.default.get('/fields/:id', 'FieldsController.showField');
    }).middleware(['rbac:venue_owner,admin']);
    Route_1.default.group(() => {
        Route_1.default.post('/venues/:id/bookings', 'ContactsController.booking').as('venues.booking');
        Route_1.default.get('/bookings', 'BookingsController.index').as('booking.index');
        Route_1.default.get('/bookings/:id', 'BookingsController.show').as('booking.show');
        Route_1.default.put('/bookings/:id/join', 'BookingsController.join').as('booking.join');
        Route_1.default.put('/bookings/:id/unjoin', 'BookingsController.unjoin').as('booking.unjoin');
        Route_1.default.get('/schedules', 'BookingsController.schedules').as('booking.schedules');
    }).middleware(['rbac:user,admin']);
}).middleware(['auth', 'verify']);
//# sourceMappingURL=routes.js.map