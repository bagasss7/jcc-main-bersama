import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'

export default class Rbac {
  public async handle ({auth,response}: HttpContextContract, next: () => Promise<void>,rules) {
    // code for middleware goes here. ABOVE THE NEXT CALL
    const roles = rules
    if(roles.length===0){
      return response.badRequest({message:`err`})
    }
    else{
      try{
        const user = auth.user?.role
        if(roles.includes(user)){
          await next()
        }
        else{
          return response.unauthorized({message:`Hanya user dengan role ${roles} yang dapat akses route ini`})
        }
      }
      catch(err){
        return response.badRequest({message:`${err}`})
      }
    }

  }
}
