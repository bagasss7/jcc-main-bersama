
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Booking from 'App/Models/Booking'
import Field from 'App/Models/Field'
import BookContactValidator from 'App/Validators/BookContactValidator'
export default class BookingsController {
  /**
 * @swagger
 * /bookings:
 *  get:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Bookings
 *    summary: Get All Bookings
 *    responses:
 *      200:
 *        description: success get all Bookings
 *        content:
 *          application/json:
 *            schemas:
 *              message:
 *                type: string
 *                example: success fetch Booking
 *              data:
 *                type: array
 *                items:
 *                  $ref:'#definitions/Booking'
 * 
 *                
 *      401:
 *        description: unauthorized
 */
  public async index ({response}: HttpContextContract) {
    const allBook = await Booking.query().select('id','play_date_start','play_date_end','user_id','field_id').preload('field',(query)=>{
      query.preload('venue')
    })
    return response.status(200).json({message:'Sukses mengambil data seluruh Booking',data:allBook}) 
  }

  public async create ({}: HttpContextContract) {
    
  }

  public async store ({request,response,params,auth}: HttpContextContract) {
    try{
      const field = await Field.findByOrFail('id',params.field_id)
      
      
      const user = auth.user!
      const payload = await request.validate(BookContactValidator)
      const newBooking = new Booking()
      newBooking.play_date_start=payload.play_date_start
      newBooking.play_date_end=payload.play_date_end
      
      newBooking.related('field').associate(field)
      user.related('myBookings').save(newBooking)
      return response.created({message:'berhasil booking'})
    }
    catch(err){
      return response.badRequest({err:`${err}`})
    }
  }
    /**
 * @swagger
 * /bookings/{id}:
 *  get:
 *    operationId: getOne
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Bookings
 *    summary: Get specific Book
 *    parameters:
 *      - name: id
 *        description: Id of the Booking
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '201':
 *          description: berhasil get specific Book
 *          
 *       '422':
 *          description: gagal get specific Book
 *          
 *    
 */
  public async show ({params,response}: HttpContextContract) {
    try{
      const books = await Booking.query().where('id',params.id).withCount('players').preload('players',(playerQuery)=>{
        playerQuery.select(['id','name','email'])
      }).firstOrFail()
      const {id,field_id,play_date_start,play_date_end,user_id,players} = books.toJSON()
      const players_count = books.$extras.players_count
      return response.ok({status:'berhasil get data booking by id',
        data:id,field_id,play_date_start,play_date_end,booking_user_id:user_id,players_count,players})

    }
    catch(err){
      return response.badRequest({err:`${err}`})
    }
  }

  public async edit ({}: HttpContextContract) {
  }

  public async update ({}: HttpContextContract) {
  }

  public async destroy ({}: HttpContextContract) {
  }

  /**
 * @swagger
 * /bookings/{id}/join:
 *  put:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Bookings
 *    summary: Join specific Book
 *    parameters:
 *      - name: id
 *        description: Id of the Booking
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '201':
 *          description: berhasil join book
 *          
 *       '422':
 *          description: gagal join Book
 *          
 *    
 */

  public async join({params,response,auth}:HttpContextContract){
    try{
      const book = await Booking.findByOrFail('id',params.id)
      
      const user = auth.user!
      
      await book.related('players').sync([user.id],false)
      return response.created({message:'berhasil join booking'})
    }
    catch(err){
      return response.badRequest({err:`${err}`})
    }
  }

    /**
 * @swagger
 * /bookings/{id}/unjoin:
 *  put:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Bookings
 *    summary: Unjoin specific Book
 *    parameters:
 *      - name: id
 *        description: Id of the Booking
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '201':
 *          description: berhasil join book
 *          
 *       '422':
 *          description: gagal join Book
 *          
 *    
 */

  public async unjoin({params,response,auth}:HttpContextContract){
    try{
      const booking = await Booking.findOrFail(params.id)
      const user = auth.user!
      await booking.related('players').detach([user.id])
      return response.created({message:'berhasil unjoin booking'})
    }
    catch(err){
      return response.badRequest({err:`${err}`})
    }
  }

    /**
 * @swagger
 * /schedules:
 *  get:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Bookings
 *    summary: Get All Schedules
 *    responses:
 *      200:
 *        description: success get all Schedules
 *        content:
 *          application/json:
 *            schemas:
 *              message:
 *                type: string
 *                example: success fetch schedules
 *              data:
 *                type: array
 *                items:
 *                  $ref:'#definitions/Booking'
 * 
 *                
 *      401:
 *        description: unauthorized
 */

  public async schedules({response,auth}:HttpContextContract){
    try{
      const user = auth.user!
      
      const books = await Booking.query().select('id','play_date_start','play_date_end','user_id','field_id').preload('field',(query)=>{
        query.preload('venue')
      }).whereHas('players',(query)=>{
        query.where('user_id',user.id)
      }).orWhere('id',user.id)
      return response.ok({status:'berhasil get schedule booking',books})

      
    }
    catch(err){
      return response.badRequest({err:`Belum ada book`})
    }
  }
}
