import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import {schema} from '@ioc:Adonis/Core/Validator'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

/**
 * @swagger
 * /register:
 *  post:
 *    tags:
 *      - Authentication
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref:'#definitions/User'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/User'
 *    responses:
 *       '201':
 *          description: user created, verify otp in email
 *       '422':
 *          description: user invalid
 */

/**
 * @swagger
 * /login:
 *  post:
 *    tags:
 *      - Authentication
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            properties:
 *              email:
 *                type: string
 *                required: true
 *              password:
 *                type: string
 *                required: true
 *        application/json:
 *          schema:
 *            properties:
 *              email:
 *                type: string
 *                required: true
 *              password:
 *                type: string
 *                required: true
 *    responses:
 *       '201':
 *          description: login berhasil
 *       '422':
 *          description: user invalid
 */

/**
 * @swagger
 * /verifikasi-otp:
 *  post:
 *    tags:
 *      - Authentication
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            properties:
 *              otp_code:
 *                type: string
 *                required: true
 *              email:
 *                type: string
 *                required: true
 *        application/json:
 *          schema:
 *            properties:
 *              otp_code:
 *                type: string
 *                required: true
 *              email:
 *                type: string
 *                required: true
 *    responses:
 *       '201':
 *          description: verifikasi berhasil
 *       '422':
 *          description: verifikasi gagal
 */

export default class AuthController {
  public async register({request,response}:HttpContextContract){
    try{
      const data = await request.validate(UserValidator)
      const newUser = await User.create(data)
      const otp_code = Math.floor(100000 + Math.random() * 900000)

      await Database.table('otp_codes').insert({otp_code,user_id:newUser.id})
      await Mail.send((message) => {
        message
          .from('admin@sanberdev.com')
          .to(data.email)
          .subject('Welcome Onboard!')
          .htmlView('emails/otp_verification', { otp_code })
      })
      return response.created({message:"User berhasil register, verifiy akunmu!"})
    }
    catch(err){
      return response.badRequest({message:`${err}`})
    }
  }
  //
  public async login({request,response, auth}:HttpContextContract){
    try{
      const userSchema = schema.create({
        email: schema.string(),
        password:schema.string()
      })
      await request.validate({ schema: userSchema })
      const email = request.input('email')
      const password = request.input('password')

      const token = await auth.use('api').attempt(email,password)
      return response.ok({message:'login berhasil!',token})
    }

    catch(err){
      return response.badRequest({message:`${err}`})
    }
  }


  public async otpConfirmation({request,response}:HttpContextContract){
    try{
      const otp_code = request.input('otp_code')
      const email = request.input('email')

      const findUser = await User.findBy('email',email)
      const otpCheck = await Database.query().from('otp_codes').where('otp_code',otp_code).first()
      if (findUser?.id === otpCheck.user_id){
        findUser!.isVerified = true
        await findUser?.save()
        return response.status(200).json({message:'Berhasil konfirmasi OTP'})
      }else{
        return response.status(400).json({message:'Gagal verifikasi OTP'})
      }
    }
    catch(err){
      return response.badRequest({message:`${err}`})
    }
  }
}
