import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import CreateContactValidator from 'App/Validators/CreateContactValidator'
import Venue from 'App/Models/Venue'
import Booking from 'App/Models/Booking'
import BookByVenueValidator from 'App/Validators/BookByVenueValidator'

/**
 * @swagger
 * /venues:
 *  get:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Venue
 *    summary: Get All Venues
 *    responses:
 *      200:
 *        description: success get all Venues
 *        content:
 *          application/json:
 *            schemas:
 *              message:
 *                type: string
 *                example: success fetch Venue
 *              data:
 *                type: array
 *                items:
 *                  $ref:'#definitions/Venue'
 * 
 *                
 *      401:
 *        description: unauthorized
 */

export default class ContractsController {
  public async index({ request,response }:HttpContextContract){
    try{
      let idSearch = request.qs().id
      if(idSearch){
        //let venueFilter = await Database.from('venues').select('*').where('id',idSearch) CARA QUERY BUILDER

        //CARA MODEL ORM
        let venueFilter = await Venue.find(idSearch)
        return response.status(200).json({message:'Sukses mengambil data',data:venueFilter})
      }
      else{
        //const showVenues = await Database.from('venues').select('*') CARA QUERY BUILDER

        //CARA MODEL ORM
        let venueFind = await Venue.all()
        return response.status(200).json({message:'Sukses mengambil data',data:venueFind})
      }
    }
    catch(err){
      return response.badRequest({errors:err.messages})
    }
  }

/**
 * @swagger
 * /venues:
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Venue
 *    summary: Post Venue
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref:'#definitions/Venue'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/Venue'
 *    responses:
 *       '201':
 *          description: berhasil menambah venue baru
 *       '422':
 *          description: gagal menambah venue
 *    
 */

  public async store({request,response}:HttpContextContract){
    try{
      
      let payload = await request.validate(CreateContactValidator)
      //CARA MODEL ORM
      let venue = await Venue.create(payload)
      //const newUser = await Database.table('venues').returning('id').insert(payload)  CARA QUERY BUILDER
      
     

      return response.created({message:'berhasil menambah Venue',newId: venue})
    }
    catch(err){
      return response.unprocessableEntity({message:'gagal menambah Venue'})
    }
  }

  /**
 * @swagger
 * /venues/{id}:
 *  get:
 *    operationId: getOne
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Venue
 *    summary: Get specific Venue
 *    parameters:
 *      - name: id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '201':
 *          description: berhasil get specific Venue
 *          schema:
 *            $ref:'#/definitions/getVenue'
 *       '422':
 *          description: gagal get specific Venue
 *          
 *    
 */

  public async show({params,response}:HttpContextContract){
    try{
      
      const venue = await Venue.query().where('id',params.id).preload('fields',(query)=>{
        query.preload('booking')
      }).firstOrFail()
      const {id,name,address,phone,fields}=venue
      return response.ok({status:'berhasil get data venue by id',data:{id,name,address,phone,fields}})
      
    }
    catch(err){
      return response.badRequest({errors:`${err}`})
    }
  }

   /**
 * @swagger
 * /venues/{id}:
 *  put:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Venue
 *    summary: Update Venue
 *    produces: 
 *      - application/json
 *    parameters:
 *      - name: id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref:'#definitions/Venue'
 *        application/json:
 *          schema:
 *            $ref: '#definitions/Venue'
 *    responses:
 *       '201':
 *          description: berhasil update
 *            
 *       '422':
 *          description: gagal update
 *          
 *    
 */


  public async update({request,response,params}:HttpContextContract){
    try{
      let id = params.id
      /* CARA QUERY BUILDER
      const updateRow = await Database.from('venues').where('id',id).update(
        {
          name: request.input('name'),
          address: request.input('address'),
          phone:request.input('phone')
        }
    )*/
    
    //CARA ORM
    let venueFilter = await Venue.findOrFail(id)
    venueFilter.name = request.input('name')
    venueFilter.address = request.input('address')
    venueFilter.phone = request.input('phone')
    venueFilter.save()
    return response.ok({message:'updated!',data:venueFilter})
    }
    catch(err){
      return response.badRequest({errors:`${err}`})
    }
  }
  
    /**
 * @swagger
 * /venues/{id}:
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Venue
 *    summary: Delete Venue
 *    parameters:
 *      - name: id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '201':
 *          description: berhasil delete venue
 *          
 *       '422':
 *          description: gagal delete Venue
 *          
 *    
 */

  public async destroy({params,response}:HttpContextContract){
    try{
      //await Database.from('venues').where('id',params.id).delete() CARA QUERY BUILDER

      //CARA model ORM
      let venueDelete = await Venue.findOrFail(params.id)
      await venueDelete.delete()
      return response.ok({message:'data deleted!'})
    }
    catch(err){
      return response.badRequest({message:`Data tidak ditemukan!`})
    }
    
  }
  
    /**
 * @swagger
 * /venues/{id}/bookings:
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Venue
 *    summary: Booking Venue
 *    produces: 
 *      - application/json
 *    parameters:
 *      - name: id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            properties:
 *              field_id:
 *                type: integer
 *                required: true
 *              play_date_start:
 *                type: datetime
 *                required: true
 *              play_date_end:
 *                type: datetime
 *                required: true
 *        application/json:
 *          schema:
 *             properties:
 *              field_id:
 *                type: integer
 *                required: true
 *              play_date_start:
 *                type: date
 *                required: true
 *              play_date_end:
 *                type: date
 *                required: true
 *    responses:
 *       '201':
 *          description: berhasil booking
 *            
 *       '422':
 *          description: gagal booking
 *          
 *    
 */

  public async booking({request,response,auth,params}:HttpContextContract){
    try{
      const fieldId = request.input('field_id')
      const venue = await Venue.find(params.id)
      const field = await venue!.related('fields').query().where('id',fieldId)
      if(field.length===0){
        return response.badRequest({message:'field tidak ada di venue ini'})
      }
      const user = auth.user!
      const payload = await request.validate(BookByVenueValidator)

      const newBooking = new Booking()
      newBooking.play_date_start=payload.play_date_start
      newBooking.play_date_end=payload.play_date_end
      
      await newBooking.related('field').associate(field[0])
      user.related('myBookings').save(newBooking)
      return response.created({message:'berhasil booking'})
      
    }
    catch(err){
      return response.unprocessableEntity({errors:'tidak ada field di venue tsb'})
    }
  }

  
}