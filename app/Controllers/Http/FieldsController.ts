import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import FieldsValidator from 'App/Validators/FieldsValidator'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'
export default class FieldsController {
    /**
 * @swagger
 * /venues/{venue_id}/fields:
 *  get:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Fields
 *    summary: Get Fields Venue
 *    parameters:
 *      - name: venue_id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '200':
 *          description: berhasil get Fields
 *          schema:
 *            $ref:'#/definitions/Field'
 *       '400':
 *          description: gagal get Fields
 *          
 *    
 */
  public async index({params, response }:HttpContextContract){
    try{
        //const showfields = await Database.from('fields').select('*').where('venue_id',params.venue_id) CARA QUERY BUILDER
        //CARA MODEL ORM
        let showFields = await Field.query().where('venue_id',params.venue_id)
        return response.status(200).json({message:'Sukses mengambil data Fields dari Venue',data:showFields})
      
    }
    catch(err){
      return response.badRequest({errors:`${err}`})
    }
  }

    /**
 * @swagger
 * /venues/{venue_id}/fields:
 *  post:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Fields
 *    summary: Create new Field for Venue
 *    parameters:
 *      - name: venue_id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            properties:
 *              name:
 *                type: string
 *                required: true
 *              type:
 *                type: string
 *                required: true
 *        application/json:
 *          schema:
 *            properties:
 *              name:
 *                type: string
 *                required: true
 *              type:
 *                type: string
 *                required: true 
 *    responses:
 *       '200':
 *          description: berhasil membuat field
 *          
 *       '400':
 *          description: gagal membuat field
 *          
 *    
 */

  public async store({params,request,response}:HttpContextContract){
    try{
      
      let payload = await request.validate(FieldsValidator)
      const venue = await Venue.findOrFail(params.venue_id)
    
      const newField = new Field()
      Object.assign(newField,payload)
 
      await newField.related('venue').associate(venue)
      return response.created({message:'berhasil menambahkan data field baru'})
    }
    catch(err){
      return response.badRequest({errors:`${err}`})
    }
  }

      /**
 * @swagger
 * /venues/{venue_id}/fields/{id}:
 *  get:
 *    operationId: getOne
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Fields
 *    summary: Get Fields Venue
 *    parameters:
 *      - name: venue_id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *      - name: id
 *        description: Id of the field
 *        in: path
 *        required: true
 *        type: integer      
 *    responses:
 *       '200':
 *          description: berhasil get Field by id
 *          schema:
 *            $ref:'#/definitions/Field'
 *       '400':
 *          description: gagal get Fields by id
 *          
 *    
 */

  public async show({params,response}:HttpContextContract){
    try{
      let id = params.id
      //let fieldsFilter = await Database.from('fields').select('*').where('venue_id',params.venue_id).andWhere('id',id).firstOrFail() CARA QUERY BUILDER

      //CARA MODEL ORM
      let fieldsFilter = await Field.query().where('venue_id',params.venue_id).andWhere('id',id).firstOrFail()
      return response.status(200).json({message:'Sukses mengambil field',data:fieldsFilter})
    }
    catch(err){
      return response.badRequest({errors:'field tidak ada'})
    }
  }

      /**
 * @swagger
 * /venues/{venue_id}/fields/{id}:
 *  put:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Fields
 *    summary: Update Field Venue
 *    parameters:
 *      - name: venue_id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *      - name: id
 *        description: Id of the field
 *        in: path
 *        required: true
 *        type: integer 
 *    requestBody:
 *      required: true
 *      content:
 *        application/x-www-form-urlencoded:
 *          schema:
 *            $ref:'#/definitions/Field'
 *        application/json:
 *          schema:
 *            $ref: '#/definitions/Field'
 *    responses:
 *       '200':
 *          description: berhasil update field
 *          
 *       '400':
 *          description: gagal update field
 *          
 *    
 */

  public async update({request,response,params}:HttpContextContract){
    try{
      let id = params.id
      /*CARA QUERY BUILDER
      const updateRow = await Database.from('fields').where('venue_id',params.venue_id).andWhere('id',id).update(
        {
          name: request.input('name'),
          type: request.input('type'),
          venue_id:request.input('venue_id')
        }
    )
    */
        
    //CARA ORM MODEL
    let fieldsFilter = await Field.query().where('venue_id',params.venue_id).andWhere('id',id).firstOrFail()
    fieldsFilter.name = request.input('name')
    fieldsFilter.type = request.input('type')
    fieldsFilter.venueId = request.input('venue_id')
    fieldsFilter.save()
    return response.ok({message:'updated!',data:fieldsFilter})
    }
    catch(err){
      return response.badRequest({errors:`${err}`})
    }
  }
  
      /**
 * @swagger
 * /venues/{venue_id}/fields/{id}:
 *  delete:
 *    security:
 *      - bearerAuth: []
 *    tags:
 *      - Fields
 *    summary: Delete Field
 *    parameters:
 *      - name: venue_id
 *        description: Id of the Venue
 *        in: path
 *        required: true
 *        type: integer
 *      - name: id
 *        description: Id of the field
 *        in: path
 *        required: true
 *        type: integer
 *        
 *    responses:
 *       '201':
 *          description: berhasil delete field
 *          
 *       '422':
 *          description: gagal delete field
 *          
 *    
 */


  public async destroy({params,response}:HttpContextContract){
    try{
      //await Database.from('fields').where('venue_id',params.venue_id).andWhere('id',params.id).delete() CARA QUERY BUILDER

      //CARA ORM MODEL
      let fieldsFilter = await Field.query().where('venue_id',params.venue_id).andWhere('id',params.id).firstOrFail()
      await fieldsFilter.delete()
      return response.ok({message:'data deleted!'})
    }
    catch(err){
      return response.badRequest({message:`Data tidak ditemukan!`})
    }
  }

  public async showField({params,response}:HttpContextContract){
    try{
      let id = params.id
      const field = await Field.query().where('id',id).preload('venue').preload('booking').firstOrFail()
      return response.ok({status:'berhasil get data booking',data:field})
    } 
    catch(err){
      return response.badRequest({message:`${err}`})
    }
  }
}