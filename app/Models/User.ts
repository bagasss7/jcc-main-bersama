
import Hash from '@ioc:Adonis/Core/Hash'
import {
  column,
  beforeSave,
  BaseModel,
  hasMany,
  HasMany,
  manyToMany,
  ManyToMany
} from '@ioc:Adonis/Lucid/Orm'
import Booking from './Booking'

/**
 * @swagger
 * definitions:
 *   User:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        email:
 *          type: string
 *        password:
 *          type: string
 *        role:
 *          type: string
 *      required:
 *        - name
 *        - email
 *        - password
 *        
 */


export default class User extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public email: string

  @column({ serializeAs: null })
  public password: string

  @column()
  public rememberMeToken?: string

  @column()
  public isVerified: boolean

  @column()
  public role: 'user' | 'venue_owner' | 'admin'

  @beforeSave()
  public static async hashPassword (user: User) {
    if (user.$dirty.password) {
      user.password = await Hash.make(user.password)
    }
  }

  @hasMany(()=> Booking)
  public myBookings: HasMany<typeof Booking>

  @manyToMany(()=>Booking)
  public books: ManyToMany<typeof Booking>

}
