
import { BaseModel, column ,belongsTo,BelongsTo, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import Venue from './Venue'
import Booking from './Booking'

/**
 * @swagger
 * definitions:
 *   Field:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        type:
 *          type: string
 *        venue_id:
 *          type: string
 *   
 *                    
 */


export default class Field extends BaseModel {
  public static table ="fields"
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public type: 'futsal' | 'mini soccer' | 'basketball' | 'volleyball' | 'soccer'

  @column()
  public venueId: number

  @belongsTo(()=>Venue)
  public venue: BelongsTo<typeof Venue>

  @hasMany(()=>Booking)
  public booking: HasMany<typeof Booking>
}
