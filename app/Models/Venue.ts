
import { BaseModel, column, hasMany, HasMany} from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'

/**
 * @swagger
 * definitions:
 *   Venue:
 *      type: object
 *      properties:
 *        name:
 *          type: string
 *        address:
 *          type: string
 *        phone:
 *          type: string
 *   getVenue:
 *      type: object
 *      required:
 *        - id
 *        - name
 *        - address
 *        - phone
 *      properties:
 *        name:
 *          type: string
 *        address:
 *          type: string
 *        phone:
 *          type: string
 *        fields:
 *          type: array
 *       
 *                    
 */
export default class Venue extends BaseModel {
  public static table ="venues"
  @column({ isPrimary: true })
  public id: number

  @column()
  public name: string

  @column()
  public address: string

  @column()
  public phone: string

  @hasMany(()=>Field)
  public fields: HasMany<typeof Field>
  
}
