import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Field from './Field'
import User from './User'

/**
 * @swagger
 * definitions:
 *   Booking:
 *      type: object
 *      properties:
 *        fieldId:
 *          type: integer
 *        play_date_start:
 *          type: string
 *        play_date_end:
 *          type: string
 *        userId:
 *          type: integer
 *       
 *                    
 */
export default class Booking extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public fieldId: number

  @column.dateTime()
  public play_date_start: DateTime

  @column.dateTime()
  public play_date_end: DateTime

  @column()
  public userId: number

  @belongsTo(()=>Field)
  public field: BelongsTo<typeof Field>

  @belongsTo(()=>User)
  public bookingUser: BelongsTo<typeof User>

  @manyToMany(()=>User, {
    pivotTable:'schedules'
  })
  public players: ManyToMany<typeof User>

  
}
