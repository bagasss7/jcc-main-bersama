import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Bookings extends BaseSchema {
  protected tableName = 'bookings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.integer('field_id').unsigned().references('fields.id').onDelete('CASCADE')
      table.dateTime('play_date_start').notNullable()
      table.dateTime('play_date_end').notNullable()
      table.integer('user_id').unsigned().references('users.id').onDelete('CASCADE')
      
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
