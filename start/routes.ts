/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

//
Route.post('/register','AuthController.register').as('auth.register')
Route.post('/login','AuthController.login').as('auth.login')
Route.post('/verifikasi-otp','Authcontroller.otpConfirmation').as('auth.otp')
Route.get('/api/hello','TestsController.hello')
Route.group(()=>{
  Route.group(()=>{
    Route.get('/venues','ContactsController.index').as('venues.index')
    Route.post('/venues','ContactsController.store').as('venues.store')
    Route.get('/venues/:id','ContactsController.show').as('venues.show')
    Route.put('/venues/:id','ContactsController.update').as('venues.update')
    Route.delete('/venues/:id','ContactsController.destroy').as('venues.destroy')
    Route.resource('venues.fields','FieldsController')
    Route.get('/fields/:id','FieldsController.showField')
  }).middleware(['rbac:venue_owner,admin'])
  
  Route.group(()=>{
    Route.post('/venues/:id/bookings','ContactsController.booking').as('venues.booking')
    Route.get('/bookings','BookingsController.index').as('booking.index')
    Route.get('/bookings/:id','BookingsController.show').as('booking.show')
    Route.put('/bookings/:id/join','BookingsController.join').as('booking.join')
    Route.put('/bookings/:id/unjoin','BookingsController.unjoin').as('booking.unjoin')
    Route.get('/schedules','BookingsController.schedules').as('booking.schedules')
  }).middleware(['rbac:user,admin'])
}).middleware(['auth','verify'])
